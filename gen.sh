rm blog/html -rf
rm blog/index.md
rm blog/reverse.md
rm feed/feed.xml
rm feed/feeditems
mkdir blog/html
for yearfol in blog/md/*/; do
  yearfol=${yearfol%/}
  year=${yearfol##*/}
  mkdir blog/html/$year
  for monthfol in blog/md/$year/*/; do
    monthfol=${monthfol%/}
    month=${monthfol##*/}
    mkdir blog/html/$year/$month
    for dayfol in blog/md/$year/$month/*/; do
      dayfol=${dayfol%/}
      day=${dayfol##*/}
      mkdir blog/html/$year/$month/$day
      for file in `find blog/md/$year/$month/$day/ -name '*.md'`; do
        name=`basename $file`
        mkdir blog/html/$year/$month/$day/${name%.md};
        cat blog/templatest.html >> blog/html/$year/$month/$day/${name%.md}/index.html;
        pandoc $file >> blog/html/$year/$month/$day/${name%.md}/index.html;
        cat blog/templatefoot.html >> blog/html/$year/$month/$day/${name%.md}/index.html;

        title=$(head -n 1 blog/md/$year/$month/$day/$name)
        head="[${title#\#}](/blog/html/$year/$month/$day/${name%.md})"
        date="$day-$month-$year"
        daterev="$year-$month-$day"
        content="$(head -n 5 blog/md/$year/$month/$day/$name | sed "/#/ d")"
        printf "::: {.panel}\n\n# $head\n\n#### $date\n$content\n\n:::\n\n" >> blog/reverse.md

        printf "::: {.panel}\n\n# $head\n\n#### $date\n$content\n\n:::\n\n" | cat - blog/index.md > blog/temp
        #<content><![CDATA[ `cat blog/html/$year/$month/$day/${name%.md}/index.html` ]]></content:encoded>
        printf "<item>
          <title>${title#\#}</title>
          <link>https://lokegaonkar.in/blog/html/$year/$month/$day/${name%.md}</link>
          <description><![CDATA[`cat blog/html/$year/$month/$day/${name%.md}/index.html`]]></description> -->
          <pubDate>`date --date=$daterev`</pubDate>
          <guid isPermaLink='false'>https://lokegaonkar.in/blog/html/$year/$month/$day/${name%.md}</guid>
          </item>" | cat - feed/feeditems > feed/temp
        mv blog/temp blog/index.md
        mv feed/temp feed/feeditems
      done;
    done;
  done;
done;

cat blog/indextemplate.html > blog/reverse.html
pandoc blog/reverse.md >> blog/reverse.html
cat blog/templatefoot.html >> blog/reverse.html

cat blog/indextemplate.html > blog/index.html
pandoc blog/index.md >> blog/index.html
cat blog/templatefoot.html >> blog/index.html

cat feed/rsshead > feed/feed.xml
cat feed/feeditems >> feed/feed.xml
printf "</channel></rss>" >> feed/feed.xml

cat indextemplate.html > index.html
pandoc index.md >> index.html
printf "</body> </html>" >> index.html
